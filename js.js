/**
 * Created by laudine on 12.02.15.
 */

// Settings ------------------------------------------------------------

var
    options = {
        "username":"vivawasserDE",
        "hash":"b49236a39c7f7763f54f09a66ae867b2",
        "numberOfItems":25,
        "itemsOnMobileDevices":10,
        "itemsOnReload":10,
        "itemsOnMobileReload":4,
        "owned":0,
        "showOlderOnScrollDown":1,
        "scroller":0
    },

    buttons = ['impressum', 'datenschutz', 'mitmachen', 'wallcontainer']
;


//----------------------------------------------------------------------
// Live Wall ---------------------------------------------------------

function initStream()
{
    var stream = new livewallStream();
    if( isMobile() )
    {
        options.numberOfItems = options.itemsOnMobileDevices;
        options.itemsOnReload = options.itemsOnMobileReload;
    }
    stream.init(options);
}

//----------------------------------------------------------------------
// Helper -------------------------------------------------------------

function isMobile()
{
   return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function setVisible(id)
{
    if(!id) { return; }
    if ($("#"+id).is(":visible")) { return false; }
    else
    {
        for(var i in buttons)
        {
            if(buttons[i] == id) { continue; }
            $('#'+buttons[i]).fadeOut();
        }
        window.scrollTo(0,0);
        $("#"+id).css( "top", $("#logo").css('height'));

        //** workaround gegen masonry / fadeIn - Problem:
        var html = $('#livewallStream').html();
        $('#livewallStream').html('').css('opacity', 0);
        $("#"+id).fadeIn('slow', function()
        {
            $('#livewallStream').html(html);
            $('#livewallStream').masonry("reload");
            $('#livewallStream').animate( {'opacity' : 1}, 1000);
        });
        //** end workaround
    }
}

//----------------------------------------------------------------------
// Handler -------------------------------------------------------------

$(document).ready(function()
{
    // set some Handler:
    $(window).on('popstate', function(e)
    {
        var id = History.act(e);
        return setVisible(id);
    });

    $("#impressumButton").click(function(e){
        setVisible('impressum');
        History.set({id : 'impressum'}).register(e);
    });

    $("#mitmachenButton").click(function(e){
        setVisible('mitmachen');
        History.set({id : 'mitmachen'}).register(e);
    });

    $("#datenschutzButton").click(function(e){
        setVisible('datenschutz');
        History.set({id : 'datenschutz'}).register(e);
    });

    $("#logopng,#home").click(function(e){
        setVisible('wallcontainer');
        History.set({id : 'wallcontainer'}).register(e);
    });

    $( window ).resize(function() {
        $("#datenschutz").css( "top", $("#logo").css('height'));
        $("#mitmachen").css( "top", $("#logo").css('height'));
        $("#impressum").css( "top", $("#logo").css('height'));
    });

});


