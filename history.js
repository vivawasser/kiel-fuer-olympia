var HistoryHelper  = (function(data)
{
    this.data = null;
    this.startValue = null;
    var self = this;

    var __construct = function(data)
    {
        self.set(data);
        history.pushState(data, '#', '#');
        self.startValue = data;
    };

    this.set = function(data)
    {
        this.data = data;
        return this;
    };

    this.register = function(e)
    {
        var event = e || window.event;
        history.pushState(this.data, event.target.textContent, event.target.href);
        return event.preventDefault();
    };

    this.act = function(e)
    {
        var event = e || window.event;
        var state = history.state;
        //alert(JSON.stringify(state));
        if(!state) { return null;}
        e.preventDefault;
        return state.id;
    };

    __construct(data);
});

var History = new HistoryHelper({id : 'wallcontainer'});

